package com.rarefill.awake.activity;

import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.rarefill.awake.R;
import com.rarefill.awake.service.AwakeService;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getCanonicalName();

    public static final String POKEMON_PACKAGE = "com.nianticlabs.pokemongo";
    private Button launchPokemon;
    private Button launchButton;
    private TextView noPokemonText;
    private Intent launchIntent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        long now = System.currentTimeMillis();
        UsageStatsManager usage = (UsageStatsManager) getSystemService(Context.USAGE_STATS_SERVICE);

        launchIntent = getPackageManager().getLaunchIntentForPackage(POKEMON_PACKAGE);
        launchButton = (Button) findViewById(R.id.launch_button);
        launchPokemon = (Button) findViewById(R.id.launch_pokemon);
        noPokemonText = (TextView) findViewById(R.id.no_pokemon_text);

        if (launchIntent == null) {
            launchButton.setVisibility(View.INVISIBLE);
            launchPokemon.setVisibility(View.INVISIBLE);
            noPokemonText.setVisibility(View.VISIBLE);
        } else {
            launchButton.setVisibility(View.VISIBLE);
            launchPokemon.setVisibility(View.VISIBLE);
            noPokemonText.setVisibility(View.GONE);
        }

        if (!usage.queryUsageStats(UsageStatsManager.INTERVAL_YEARLY, now - (60 * 1000), now).isEmpty()) {
            if (launchIntent != null) {
                startService(new Intent(MainActivity.this, AwakeService.class));
                startActivity(launchIntent);
                finish();
            }
        } else {

            launchPokemon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this)
                            .setMessage("Pokemon Go will automatically launch when starting this app from now on")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (launchIntent != null) {
                                        startService(new Intent(MainActivity.this, AwakeService.class));
                                        startActivity(launchIntent);
                                        finish();
                                    }
                                }
                            });
                    builder.show();
                }
            });

            launchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this)
                                .setMessage("You must grant " + getString(R.string.app_name) + " Usage Access " +
                                        "on the next screen, or this app will not work. Return here " +
                                        "to launch Pokemon Go!")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));
                                    }
                                });

                        builder.show();
                    }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (launchPokemon != null) {
            long now = System.currentTimeMillis();
            UsageStatsManager usage = (UsageStatsManager) getSystemService(Context.USAGE_STATS_SERVICE);
            launchIntent = getPackageManager().getLaunchIntentForPackage(POKEMON_PACKAGE);

            if (launchIntent == null) {
                launchButton.setVisibility(View.INVISIBLE);
                launchPokemon.setVisibility(View.INVISIBLE);
                noPokemonText.setVisibility(View.VISIBLE);
            } else {
                launchButton.setVisibility(View.VISIBLE);
                launchPokemon.setVisibility(View.VISIBLE);
                noPokemonText.setVisibility(View.GONE);
            }

            if (!usage.queryUsageStats(UsageStatsManager.INTERVAL_YEARLY, now - (60 * 1000), now).isEmpty()) {
                launchPokemon.setEnabled(true);
                launchPokemon.setBackgroundColor(ContextCompat.getColor(this, R.color.md_light_green_A200));
                launchButton.setVisibility(View.GONE);
            }
        }
    }
}
