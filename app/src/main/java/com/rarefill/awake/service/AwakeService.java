package com.rarefill.awake.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.usage.UsageEvents;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.rarefill.awake.R;
import com.rarefill.awake.activity.MainActivity;

/**
 * Created by Phillip on 7/9/2016.
 */
public class AwakeService extends Service {
    private static final String TAG = AwakeService.class.getCanonicalName();

    private static final int NOTIFICATION_ID = 2815;
    private static final String DESTROY = "DESTROY";
    private static final long WAKE_LOCK_TIMEOUT = 30000;

    private UsageStatsManager usage;
    private Handler handler;
    private PowerManager.WakeLock wakeLock;
    private Runnable wakeRunnable;
    private NotificationCompat.Builder builder;
    private NotificationManager notificationManager;
    private PendingIntent pendingDestructionIntent;

    @Override
    public void onCreate() {
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ON_AFTER_RELEASE,
                TAG);
        usage = (UsageStatsManager) getSystemService(Context.USAGE_STATS_SERVICE);
        handler = new Handler();

        Intent destroyIntent = new Intent(this, AwakeService.class);
        destroyIntent.setAction(DESTROY);
        pendingDestructionIntent = PendingIntent.getService(this, 0, destroyIntent, 0);

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null && intent.getAction() != null && intent.getAction().equals(DESTROY)) {
            stopForeground(true);
            stopSelf();
        } else {
            builder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_stat_pokeawakenotify)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                    .setOngoing(true)
                    .addAction(android.R.drawable.ic_lock_power_off, "Turn Off", pendingDestructionIntent)
                    .setContentTitle(getString(R.string.app_name) + " is active");

            try {
                if (wakeRunnable != null) {
                    handler.removeCallbacks(wakeRunnable);
                }
                wakeRunnable = new WakeRunnable(new PokemonActivityListener() {
                    @Override
                    public void onMoveToForeground() {
                        builder.setSmallIcon(R.drawable.ic_stat_pokeawakenotify);
                        builder.setContentTitle(getString(R.string.app_name) + " is active");
                        startForeground(NOTIFICATION_ID, builder.build());

                        if (wakeLock.isHeld()) {
                            wakeLock.release();
                            wakeLock.acquire(WAKE_LOCK_TIMEOUT);
                        } else {
                            wakeLock.acquire(WAKE_LOCK_TIMEOUT);
                        }
                    }

                    @Override
                    public void onMoveToBackground() {
                        builder.setSmallIcon(R.drawable.ic_stat_pokeawakenotifyoff);
                        builder.setContentTitle(getString(R.string.app_name) + " is suspended");

                        startForeground(NOTIFICATION_ID, builder.build());

                        if (wakeLock.isHeld()) {
                            wakeLock.release();
                        }
                    }

                    @Override
                    public void onFinish() {
                        notificationManager.cancel(NOTIFICATION_ID);
                        stopForeground(true);

                        if (wakeLock.isHeld()) {
                            wakeLock.release();
                        }
                        stopSelf();
                    }
                });

                if (!wakeLock.isHeld()) {
                    wakeLock.acquire(WAKE_LOCK_TIMEOUT);
                }

                startForeground(NOTIFICATION_ID, builder.build());
                checkForPokemonActivity();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return Service.START_STICKY;
    }

    private void checkForPokemonActivity() {
        handler.postDelayed(wakeRunnable, 15000);
    }

    @Override
    public void onDestroy() {
        if (notificationManager != null) {
            stopForeground(true);
            notificationManager.cancel(NOTIFICATION_ID);
        }
        if (wakeRunnable != null) {
            handler.removeCallbacks(wakeRunnable);
        }
        if (wakeLock.isHeld()) {
            wakeLock.release();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    class WakeRunnable implements Runnable {

        private PokemonActivityListener activityListener;
        private int inactivity = 0;
        private boolean active = true;
        private ShallowEventCopy shallowEventCopy = null;

        WakeRunnable(PokemonActivityListener activityListener) {
            this.activityListener = activityListener;
        }

        @Override
        public void run() {
            long now = System.currentTimeMillis();
            UsageEvents events = usage.queryEvents(now - (15 * 1000), now);

            UsageEvents.Event event = new UsageEvents.Event();

            while (events.getNextEvent(event)) {
                //Log.d("ZZZ", event.getPackageName() + " " + event.getTimeStamp() + " " + event.getEventType());
                if (event.getPackageName().equals(MainActivity.POKEMON_PACKAGE)) {
                    if (shallowEventCopy == null) {
                        shallowEventCopy = new ShallowEventCopy(event.getEventType(), event.getTimeStamp());
                    } else if (shallowEventCopy.getTimeStamp() < event.getTimeStamp()) {
                        shallowEventCopy = new ShallowEventCopy(event.getEventType(), event.getTimeStamp());
                    }
                }
            }

            if (shallowEventCopy != null) {
                switch (shallowEventCopy.getEventType()) {
                    case UsageEvents.Event.MOVE_TO_FOREGROUND:
                        active = true;
                        inactivity = 0;
                        activityListener.onMoveToForeground();
                        break;

                    case UsageEvents.Event.MOVE_TO_BACKGROUND:
                        active = false;
                        activityListener.onMoveToBackground();
                        break;
                }
            }

            shallowEventCopy = null;

            //if not active, add 15 seconds to the inactivity counter
            if (!active) {
                inactivity = inactivity + 15;
            } else {
                if (wakeLock.isHeld()) {
                    wakeLock.release(PowerManager.ON_AFTER_RELEASE);
                    wakeLock.acquire(WAKE_LOCK_TIMEOUT);
                }
            }

            //if we are inactive for 2 minutes, kill the service
            if (inactivity > 120) {
                activityListener.onFinish();
            } else {
                checkForPokemonActivity();
            }
        }
    }

    interface PokemonActivityListener {
        void onMoveToForeground();
        void onMoveToBackground();
        void onFinish();
    }

    class ShallowEventCopy {

        private int eventType;
        private long timeStamp;

        ShallowEventCopy(int eventType, long timeStamp) {
            this.eventType = eventType;
            this.timeStamp = timeStamp;
        }

        public long getTimeStamp() {
            return timeStamp;
        }

        public int getEventType() {
            return eventType;
        }
    }

}
